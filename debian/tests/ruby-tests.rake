exclude = %w[
  ruby/rack/loader_spec.rb
  ruby/rack/preloader_spec.rb
]

config = File.absolute_path(File.join(File.dirname(__FILE__), 'config.json'))

task :default do
  chdir 'test' do
    cp config, '.'
    tests = Dir["ruby/**/*_spec.rb"] - exclude
    ruby *(%w[-S rspec -c -f d --tty -P dont-autoload-anything] + tests)
  end
end
